module.exports = function (wallaby) {
  return {
    files: [
      "index.ts",
      "package.json",
      "package-lock.json",
      "src/**/*.ts",
      "test/**/*.ts",
      "!test/**/*.spec.ts",
    ],
    tests: ["test/**/*spec.ts"],
    compilers: {
      "**/*.ts?(x)": wallaby.compilers.typeScript({ module: "commonjs" }),
    },
    setup: function (wallaby) {},
    env: {
      type: "node",
      runner: "node",
      params: {},
    },
    testFramework: "ava",
    debug: true,
    runAllTestsInAffectedTestFile: true,
  };
};
