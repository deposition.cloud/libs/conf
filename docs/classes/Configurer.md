[@deposition.cloud/conf](../README.md) / [Exports](../modules.md) / Configurer

# Class: Configurer

Class for managing application configuration.
This class provides a unified interface for accessing configuration data from various sources.

## Implements

- `PackageLockJson`

## Table of contents

### Constructors

- [constructor](Configurer.md#constructor)

### Properties

- [#nconf](Configurer.md##nconf)
- [consts](Configurer.md#consts)
- [name](Configurer.md#name)
- [version](Configurer.md#version)

### Methods

- [get](Configurer.md#get)
- [required](Configurer.md#required)

## Constructors

### constructor

• **new Configurer**(`options?`): [`Configurer`](Configurer.md)

Creates a new Configurer instance.
This constructor initializes the nconf instance and loads configuration data from the specified sources.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `options` | `any` | Configuration options. |

#### Returns

[`Configurer`](Configurer.md)

#### Defined in

[index.ts:45](https://gitlab.com/deposition.cloud/libs/conf/-/blob/f1fd2ae/src/index.ts#L45)

## Properties

### #nconf

• `Private` **#nconf**: `any`

The nconf instance used for configuration management.

#### Defined in

[index.ts:38](https://gitlab.com/deposition.cloud/libs/conf/-/blob/f1fd2ae/src/index.ts#L38)

___

### consts

• **consts**: `any` = `{}`

An object of constant configuration values.

#### Defined in

[index.ts:32](https://gitlab.com/deposition.cloud/libs/conf/-/blob/f1fd2ae/src/index.ts#L32)

___

### name

• **name**: `string` = `""`

The name of the package, read from package-lock.json.

#### Implementation of

PackageLockJson.name

#### Defined in

[index.ts:22](https://gitlab.com/deposition.cloud/libs/conf/-/blob/f1fd2ae/src/index.ts#L22)

___

### version

• **version**: `string` = `""`

The version of the package, read from package-lock.json.

#### Implementation of

PackageLockJson.version

#### Defined in

[index.ts:27](https://gitlab.com/deposition.cloud/libs/conf/-/blob/f1fd2ae/src/index.ts#L27)

## Methods

### get

▸ **get**(`key`): `any`

Get a configuration value by key.
This method retrieves the value for a given key from the configuration.
If the value is an object with numeric keys, it is treated as an array.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `any` | The key of the configuration value. |

#### Returns

`any`

The configuration value.

#### Defined in

[index.ts:118](https://gitlab.com/deposition.cloud/libs/conf/-/blob/f1fd2ae/src/index.ts#L118)

___

### required

▸ **required**(`keys`): `void`

Check if the given keys are present in the configuration.
This method ensures that all required configuration keys are present.
If any are missing, an error is thrown.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `keys` | `any` | The keys to check. |

#### Returns

`void`

#### Defined in

[index.ts:142](https://gitlab.com/deposition.cloud/libs/conf/-/blob/f1fd2ae/src/index.ts#L142)
