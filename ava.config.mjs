export default {
  extensions: ["ts"],
  files: ["test/**/*.spec.ts"],
  require: ["ts-node/register"],
  verbose: true,
};
