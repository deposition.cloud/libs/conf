import fs from "fs";
import path from "path";
import nconf from "nconf";

/**
 * Interface for package-lock.json file structure.
 * This is used to ensure the correct structure when reading the package-lock.json file.
 */
interface PackageLockJson {
  name: string;
  version: string;
}

/**
 * Class for managing application configuration.
 * This class provides a unified interface for accessing configuration data from various sources.
 */
class Configurer implements PackageLockJson {
  /**
   * The name of the package, read from package-lock.json.
   */
  name: string = "";

  /**
   * The version of the package, read from package-lock.json.
   */
  version: string = "";

  /**
   * An object of constant configuration values.
   */
  consts: any = {};

  /**
   * The nconf instance used for configuration management.
   * @private
   */
  #nconf: any;

  /**
   * Creates a new Configurer instance.
   * This constructor initializes the nconf instance and loads configuration data from the specified sources.
   * @param {object} options - Configuration options.
   */
  constructor(options: any = {}) {
    options = {
      ...{
        packageLockJsonFilePath: "package-lock.json",
        required: [],
        defaults: {},
        files: [],
        overrides: {},
        consts: {},
        separator: "__",
      },
      ...options,
    };

    try {
      const pkg: PackageLockJson = JSON.parse(
        fs.readFileSync(options.packageLockJsonFilePath, "utf8"),
      );
      this.name = pkg.name;
      this.version = pkg.version;

      const files = options.files.filter((file) => {
        if (!fs.existsSync(file)) {
          console.log(
            `${this.name} WARN: ignoring configuration file: ${file}`,
          );
          return false;
        }
        return true;
      });

      nconf.formats.yaml = require("nconf-yaml");

      nconf.use("memory");

      nconf.overrides(options.overrides);

      nconf.argv();

      nconf.env({
        separator: options.separator,
        parseValues: true,
      });

      files.forEach((file) => {
        const fileOptions: any = { file };

        // try yaml first
        if (path.extname(file) === ".yml" || path.extname(file) === ".yaml") {
          fileOptions.format = nconf.formats.yaml;
        } // else assume json

        nconf.file(file, fileOptions);
      });

      nconf.defaults(options.defaults);

      nconf.required(options.required);

      this.#nconf = nconf;
    } catch (error) {
      console.log(error);
      process.exit(1);
    }
  }

  /**
   * Get a configuration value by key.
   * This method retrieves the value for a given key from the configuration.
   * If the value is an object with numeric keys, it is treated as an array.
   * @param {string} key - The key of the configuration value.
   * @returns {any} The configuration value.
   */
  get (key) {
    const confObj = this.#nconf.get(key);
    if (
      confObj !== undefined &&
      confObj !== null &&
      typeof confObj !== "string"
    ) {
      const keys = Object.keys(confObj);
      const isArray = keys.some((key) => !Number.isNaN(Number(key)));

      return isArray
        ? Array.from(Object.assign({}, confObj, { length: keys.length }))
        : confObj;
    } else {
      return confObj;
    }
  }

  /**
   * Check if the given keys are present in the configuration.
   * This method ensures that all required configuration keys are present.
   * If any are missing, an error is thrown.
   * @param {string[]} keys - The keys to check.
   */
  required (keys) {
    this.#nconf.required(keys);
  }
}

export { Configurer };
