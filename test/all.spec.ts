import test from "ava";
import { Configurer } from "../src";
import fs from "fs";
import crypto from "crypto";

const random = crypto.randomBytes(6).toString("hex");

const tmp = "test/tmp/";
const yamlConfigFile = `${tmp}default-${random}.yml`;
const jsonConfigFile = `${tmp}default-${random}.json`;

const defaultString = "default";
const overrideString = "override";
const expectedFooValue = "barJSON";
const expectedOnlyInYAML = "fileYAML";

test.before((t) => {
  if (!fs.existsSync(tmp)) {
    fs.mkdirSync(tmp);
  }

  fs.writeFileSync(
    yamlConfigFile,
    `foo: bar\nonlyInYAML: ${expectedOnlyInYAML}`,
  );
  fs.writeFileSync(jsonConfigFile, '{ "foo": "barJSON", "onlyInJSON": "xyz" }');
});

test.after((t) => {
  fs.unlinkSync(yamlConfigFile);
  fs.unlinkSync(jsonConfigFile);
});

test("multiple config files", (t) => {
  const config = new Configurer({
    defaults: { foo: "default" },
    files: [jsonConfigFile, yamlConfigFile],
    overrides: { onlyInJSON: overrideString },
  });

  t.is(config.get("onlyInYAML"), expectedOnlyInYAML);
  t.not(config.get("foo"), defaultString);
  t.is(config.get("foo"), expectedFooValue);
  t.is(config.get("onlyInJSON"), overrideString);
});
