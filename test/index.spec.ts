import test from "ava";
import { Configurer } from "../src";
import fs from "fs";
import crypto from "crypto";

const random = crypto.randomBytes(6).toString("hex");

const tmp = "test/tmp/";
const yamlConfigFile = `${tmp}default-${random}.yml`;
const jsonConfigFile = `${tmp}default-${random}.json`;

test.before((t) => {
  if (!fs.existsSync(tmp)) {
    fs.mkdirSync(tmp);
  }

  fs.writeFileSync(yamlConfigFile, "foo: bar\nonlyInYAML: abc");
  fs.writeFileSync(jsonConfigFile, '{ "foo": "barJSON", "onlyInJSON": "xyz" }');
});

test.after((t) => {
  fs.unlinkSync(yamlConfigFile);
  fs.unlinkSync(jsonConfigFile);
});

test("ava setup", (t) => {
  t.pass();
});

test("empty constructor", (t) => {
  const pkg = { name: "@deposition.cloud/conf" };

  const config = new Configurer();

  t.is(pkg.name, config.name);
});

test("nonempty constructor", (t) => {
  const pkg = { name: "@deposition.cloud/conf" };

  const config = new Configurer({ packageLockJsonFilePath: "package.json" });

  t.is(pkg.name, config.name);
});

test("env var should recognize booleans", (t) => {
  process.env.boolean = "false";
  const expected = false;

  const conf = new Configurer();

  t.is(conf.get("boolean"), expected);
});

test("env var json array of values", (t) => {
  process.env.json_arr_values =
    '["great", "day", "today!", 42,33.5, true, "undefined", null, false, "false"]';
  const conf = new Configurer();

  const expected = [
    "great",
    "day",
    "today!",
    42,
    33.5,
    true,
    "undefined",
    null,
    false,
    "false",
  ];

  t.deepEqual(conf.get("json_arr_values"), expected);
});

test("env var json array of objects", (t) => {
  process.env.json_arr_objects =
    '[{ "foo": "great", "bar": "day" }, { "foo": "today!" }]';
  const expected = [{ foo: "great", bar: "day" }, { foo: "today!" }];

  const conf = new Configurer();

  t.deepEqual(conf.get("json_arr_objects"), expected);
});

test("env var json object", (t) => {
  process.env.json_object =
    '{ "foo": "great", "bar": "day", "foobar": ["today!"] }';
  const expected = { foo: "great", bar: "day", foobar: ["today!"] };

  const conf = new Configurer();

  t.deepEqual(conf.get("json_object"), expected);
});

test("env var arrays", (t) => {
  process.env.arr__0__foo = "great";
  process.env.arr__0__bar = "day";
  process.env.arr__1__foo = "today!";
  const expected = [{ foo: "great", bar: "day" }, { foo: "today!" }];

  const conf = new Configurer();

  t.deepEqual(conf.get("arr"), expected);
});

test("env var arrays with objects", (t) => {
  process.env.arr2__0__foo__zap = "great";
  process.env.arr2__0__foo__bar = "day";
  process.env.arr2__1__foo = "today!";
  const expected = [{ foo: { zap: "great", bar: "day" } }, { foo: "today!" }];

  const conf = new Configurer();

  t.deepEqual(conf.get("arr2"), expected);
});

test("returns undefined when var not defined", (t) => {
  const conf = new Configurer();

  t.is(conf.get("UNDEFINED_VARIABLE_XYZ"), undefined);
});

test("yaml config file", (t) => {
  const expectedFooValue = "bar";

  const config = new Configurer({ files: [yamlConfigFile] });

  t.is(config.get("foo"), expectedFooValue);
});
