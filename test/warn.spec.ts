import test from "ava";
import sinon from "sinon";
import { Configurer } from "../src";

test("missing config file warns", (t) => {
  const stub = sinon.stub(console, "log");

  const configurer = new Configurer({ files: ["./config/does-not-exist.yml"] });

  t.is(configurer, configurer);
  t.true(stub.calledOnce);
});
