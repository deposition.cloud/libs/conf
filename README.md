# @deposition.cloud/conf

A configuration management library for Node.js applications, providing sane defaults for a self-hosted cloud native world. It offers a unified interface for accessing configuration data from various sources such as environment variables, command-line arguments, configuration files, and more.

## Installation

If not configured already, add the GitLab npm registry for the `@deposition.cloud` organizational scope:

```bash
echo @deposition.cloud:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

Then, install the package using npm:

```bash
npm install --save @deposition.cloud/conf
```

## Usage

Here's a basic example of how to use the library:

```javascript
import { Configurer } from "@deposition.cloud/conf";

// Create a new Configurer instance with your configuration options
const config = new Configurer({
  packageLockJsonFilePath: "package-lock.json",
  required: ["REQUIRED_KEY"],
  defaults: { DEFAULT_KEY: "default_value" },
  files: ["path/to/config.json"],
  overrides: { OVERRIDE_KEY: "override_value" },
  consts: { CONST_KEY: "const_value" },
  separator: "__",
});

// Get a configuration value by key
const value = config.get("KEY");
```

## Documentation

For more detailed information about the API and its usage, please refer to the [API Documentation](https://deposition.cloud.gitlab.io/libs/conf).

## Test and Develop

To run tests with AVA, clone the repository and run the test command:

```bash
git clone git@gitlab.com:deposition.cloud/libs/conf.git
npm test
```

### Local CI

GitLab-CI Local requires a Docker Registry. To start one, use the following command:

```bash
docker run -d -p 5000:5000 --restart=always --name local-registry registry
```

Then, update your `/etc/hosts` file to use your network (not localhost) IP, e.g.:

```txt
172.20.224.27   local-registry
```

## Continuous Deployment

Please refer to the [`.gitlab-ci.yml`](./.gitlab-ci.yml) pipeline configuration for details.

## License

This project is ethically sourced under the [Atmosphere License](https://www.open-austin.org/atmosphere-license/). It's like open source, for good.
